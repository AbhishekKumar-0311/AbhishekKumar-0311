### Hi there 👋

<!--
**AbhishekKumar-0311/AbhishekKumar-0311** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

- 🔭 I’m currently working on Exploratory Data Analysis.
- 🌱 I’m currently pursuing Post-Graduation in Machine Learning and Artificial Intelligence.
- 👯 I’m looking to collaborate on Kaggle competitions.
- 🤔 I’m looking for help to start with hands-on project in Machine Learning.
- 📫 How to reach me: https://bit.ly/AKSLinkedIn


[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=AbhishekKumar-0311&layout=compact)](https://github.com/anuraghazra/github-readme-stats)
<!-- <br>
<!-- ![Anurag's github stats](https://github-readme-stats.vercel.app/api?username=AbhishekKumar-0311&hide=contribs,prs&show_icons=true&theme=radical)


<!-- [![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=AbhishekKumar-0311&repo=github-readme-stats)](https://github.com/anuraghazra/github-readme-stats)
<!-- ![Anurag's github stats](https://github-readme-stats.vercel.app/api?username=AbhishekKumar-0311&hide=contribs,prs) -->


